import sys, os
import re

class SymbolTable:
    def __init__(self):
        self.table = list()
        self.table.append(("SP", 0))
        self.table.append(("LCL", 1))
        self.table.append(("ARG", 2))
        self.table.append(("THIS", 3))
        self.table.append(("THAT", 4))
        self.table.append(("R0", 0))
        self.table.append(("R1", 1))
        self.table.append(("R2", 2))
        self.table.append(("R3", 3))
        self.table.append(("R4", 4))
        self.table.append(("R5", 5))
        self.table.append(("R6", 6))
        self.table.append(("R7", 7))
        self.table.append(("R8", 8))
        self.table.append(("R9", 9))
        self.table.append(("R10", 10))
        self.table.append(("R11", 11))
        self.table.append(("R12", 12))
        self.table.append(("R13", 13))
        self.table.append(("R14", 14))
        self.table.append(("R15", 15))
        self.table.append(("SCREEN", 16384))
        self.table.append(("KBD", 24576))

    def addEntry(self, symbol, address):
        self.table.append((symbol, address))
    
    def contains(self, symbol):
        symbols = [w[0] for w in self.table]
        
        if symbol in symbols:
            return 1
        else:
            return 0
    
    def getAddress(self, symbol):
        symbols = [w[0] for w in self.table]
        address = [w[1] for w in self.table]
        
        return address[symbols.index(symbol)]

class Parser:
    def __init__(self, nom_f):
        self.file = open(nom_f, "r")
        self.content = self.file.readlines()
        self.commands = list()
        self.line = 0
        
    def firstPass(self, table):
        for val in self.content:
            if not re.match("//", val):
                if re.search("(.*)(//.*)*", val):
                    if val.strip():
                        command = re.search("([\(\)A-Z_ \.$;a-z@=0-9\-\+!&|]+)(.*)", val).group(1)
                        if command:
                            self.commands.append(command.strip())

        line = 0
        for val in self.commands:
            if val[0] == "(":
                table.addEntry(val[1:-1], line)
            else:
                line += 1        
             
    def hasMoreCommands(self):
        if self.line < len(self.commands):
            return True
        else:
            return False
        
    def advance(self):
        self.line += 1
        
    def commandType(self):
        command = self.commands[self.line]
        
        if re.match("@", command):
            return "A_COMMAND"
        elif re.search("=|;", command):
            return "C_COMMAND"
        else:
            return "L_COMMAND"
    
    def symbol(self):
        command = self.commands[self.line]
        if re.match("@", command):
            return command[1:]
        else:
            return command

    def dest(self):
        command = self.commands[self.line]
        
        if re.search("=", command):
            dest = command.split("=")[0]
        else:
            dest = None
       
        return dest

    def comp(self):
        command = self.commands[self.line]
       
        if re.search(";", command) and re.search("=", command):
            comp = command.split("=")[1].split(";")[0]
        elif re.search(";", command):
            comp = command.split(";")[0]
        else:
            comp = command.split("=")[1]
       
        return comp
    
    def jump(self):
        command = self.commands[self.line]
               
        if re.search(";", command):
            jump = command.split(";")[1]
            return jump
        else:
            return None

   
class Code:
    def dest(mnemo):
        if mnemo:
            if mnemo == "M":
                return("001")
            elif mnemo == "D":
                return("010")
            elif mnemo == "MD":
                return("011")
            elif mnemo == "A":
                return("100")
            elif mnemo == "AM":
                return("101")
            elif mnemo == "AD":
                return("110")
            else: # mnemo == "AMD"
                return("111")
        else:
            return "000"
            
    def comp(mnemo):
        if mnemo == "0":
            return "0101010"
            
        elif mnemo == "1":
            return "0111111"
            
        elif mnemo == "-1":
            return "0111010"
            
        elif mnemo == "D":
            return "0001100"
            
        elif mnemo == "A":
            return "0110000"
            
        elif mnemo == "!D":
            return "0001101"
            
        elif mnemo == "!A":
            return "0110001"
            
        elif mnemo == "-D":
            return "0001111"
            
        elif mnemo == "-A":
            return "0110011"
            
        elif mnemo == "D+1":
            return "0011111"
            
        elif mnemo == "A+1":
            return "0110111"
            
        elif mnemo == "D-1":
            return "0001110"
            
        elif mnemo == "A-1":
            return "0110010"
            
        elif mnemo == "D+A":
            return "0000010"
            
        elif mnemo == "D-A":
            return "0010011"
            
        elif mnemo == "A-D":
            return "0000111"
            
        elif mnemo == "D&A":
            return "0000000"
            
        elif mnemo == "D|A":
            return "0010101"
            
        elif mnemo == "M":
            return "1110000"
            
        elif mnemo == "!M":
            return "1110001"
            
        elif mnemo == "-M":
            return "1110011"
                        
        elif mnemo == "M+1":
            return "1110111"
            
        elif mnemo == "M-1":
            return "1110010"
            
        elif mnemo == "D+M":
            return "1000010"
            
        elif mnemo == "D-M":
            return "1010011"
            
        elif mnemo == "M-D":
            return "1000111"
            
        elif mnemo == "D&M":
            return "1000000"
            
        else: # mnemo == "D|M"
            return "1010101"
            
    def jump(mnemo):
        if mnemo:
            if mnemo == "JGT":
                return "001"
            elif mnemo == "JEQ":
                return "010"
            elif mnemo == "JGE":
                return "011"
            elif mnemo == "JLT":
                return "100"
            elif mnemo == "JNE":
                return "101"
            elif mnemo == "JLE":
                return "110"
            else: # mnemo == "JMP"
                return "111"
        else:
            return "000"

            
class Write:
    def __init__(self, nom_f):
        nom_f = nom_f.replace(".asm", "") + ".hack"
        self.file = open(nom_f, "w+")
        
    def write_s(self, address):    
        self.file.write("0" * (16-len(bin(int(address))[2:])) + bin(int(address))[2:] + "\n")
        
    def write_c(self, d, c, j):
        comp = Code.comp(c)
        dest = Code.dest(d)
        jump = Code.jump(j)

        self.file.write("111" + comp + dest + jump + "\n")

if __name__ == "__main__":

    table = SymbolTable()

    parser = Parser(sys.argv[1])
    parser.firstPass(table)

    parser.line = 0

    write = Write(sys.argv[1])
    
    length = len(table.table)
    
    loop = 1
    while loop:
        if parser.hasMoreCommands():
            type = parser.commandType()
            if type == "A_COMMAND":
                symbol = parser.symbol()
                
                try:
                    address = int(symbol, 10)
                except:               
                    if table.contains(symbol):
                        address = table.getAddress(symbol)
                    else:
                        table.addEntry(symbol, len(table.table) - length + 16)
                        address = len(table.table) - length + 15 # one added
                           
                write.write_s(address)
            
            elif type == "L_COMMAND":
                pass
            
            else:
                dest = parser.dest()
                comp = parser.comp()
                jump = parser.jump()
                
                write.write_c(dest, comp, jump)
            
            parser.advance() 
        else:
            loop = 0